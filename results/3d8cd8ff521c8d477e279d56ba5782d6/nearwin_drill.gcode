M201 X400 Y400 Z200 ; sets maximum accelerations, mm/sec^2
M203 X50 Y50 Z15 ; set velocity limit
G90 ; use absolute coordinates

G92 X0 Y0 Z10.000000 ; calibrate

G1 X179.666667 Y388.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X182.000000 Y382.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X175.500000 Y382.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.333333 Y390.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X166.166667 Y386.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X164.666667 Y393.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X157.500000 Y390.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X149.833333 Y392.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X143.833333 Y390.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X141.666667 Y396.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X135.833333 Y399.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X141.500000 Y403.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X149.166667 Y400.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X150.000000 Y408.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X157.500000 Y406.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X156.833333 Y398.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X165.500000 Y401.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X168.833333 Y409.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X175.166667 Y412.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X179.166667 Y407.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X174.166667 Y403.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X183.333333 Y395.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X187.666667 Y401.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X185.666667 Y407.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X192.166667 Y406.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X198.166667 Y409.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X200.666667 Y415.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X198.000000 Y421.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X202.666667 Y427.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X207.166667 Y432.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X203.666667 Y438.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X209.833333 Y440.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X204.833333 Y444.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X211.833333 Y449.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X218.166667 Y443.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X222.500000 Y438.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X216.333333 Y435.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X221.166667 Y429.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X226.666667 Y425.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X228.166667 Y432.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.666667 Y435.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X243.166667 Y435.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X249.166667 Y438.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X256.166667 Y439.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X262.666667 Y439.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X264.333333 Y446.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.166667 Y454.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X267.000000 Y461.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X261.666667 Y465.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.166667 Y471.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X251.833333 Y473.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X252.333333 Y466.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X253.833333 Y459.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X254.333333 Y452.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X247.000000 Y452.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X251.166667 Y445.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X240.666667 Y442.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X232.000000 Y441.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X231.833333 Y448.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X238.000000 Y451.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X233.833333 Y457.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X226.000000 Y456.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X226.833333 Y463.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X232.500000 Y466.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X239.333333 Y465.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X245.166667 Y462.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X243.166667 Y472.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X235.500000 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X226.666667 Y473.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X222.166667 Y468.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X213.833333 Y471.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X208.000000 Y468.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X212.166667 Y463.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X207.333333 Y457.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X199.500000 Y460.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X193.000000 Y461.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X189.000000 Y468.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X183.666667 Y474.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X173.666667 Y470.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X169.333333 Y463.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X165.000000 Y470.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X154.833333 Y469.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X150.333333 Y463.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X152.833333 Y454.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X148.333333 Y448.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X139.666667 Y448.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X141.166667 Y440.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.666667 Y434.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X150.166667 Y426.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X144.166667 Y420.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X139.333333 Y426.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X130.666667 Y420.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X128.166667 Y428.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X132.500000 Y435.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X130.833333 Y445.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X127.666667 Y453.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X133.333333 Y459.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X129.000000 Y465.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X128.666667 Y472.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X136.500000 Y471.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X141.333333 Y465.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X143.000000 Y457.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.666667 Y473.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.166667 Y460.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X166.666667 Y455.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.000000 Y447.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X164.166667 Y439.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X170.833333 Y435.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X166.500000 Y429.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.000000 Y422.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X163.000000 Y415.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X169.500000 Y419.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X176.500000 Y426.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X180.500000 Y419.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X188.666667 Y425.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X184.000000 Y430.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X187.333333 Y436.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X192.333333 Y440.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X196.000000 Y445.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X191.500000 Y451.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X199.166667 Y453.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X201.333333 Y467.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X203.000000 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X196.333333 Y474.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X181.166667 Y462.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X184.500000 Y455.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X176.500000 Y455.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X178.166667 Y447.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X185.833333 Y444.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X178.166667 Y438.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X169.833333 Y445.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X153.000000 Y440.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X158.166667 Y430.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X152.833333 Y416.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X138.333333 Y413.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X133.833333 Y406.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X127.000000 Y403.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X120.833333 Y406.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X121.833333 Y399.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X112.833333 Y393.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X107.666667 Y401.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X113.000000 Y408.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X107.000000 Y411.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X111.666667 Y416.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X118.000000 Y414.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X120.166667 Y421.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X119.166667 Y429.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X114.000000 Y436.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X122.833333 Y438.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X118.000000 Y447.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X117.833333 Y455.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X121.333333 Y463.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X120.500000 Y473.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X112.166667 Y473.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X102.500000 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X96.666667 Y471.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X102.166667 Y468.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X100.666667 Y458.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X94.166667 Y461.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X89.833333 Y454.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X92.000000 Y448.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X85.500000 Y448.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X83.333333 Y441.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X74.500000 Y442.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X68.500000 Y440.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X72.333333 Y434.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X65.833333 Y433.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X58.500000 Y438.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X50.333333 Y441.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X43.833333 Y440.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X43.833333 Y446.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X37.500000 Y448.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X32.500000 Y453.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y451.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.833333 Y445.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X32.166667 Y439.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y432.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X26.833333 Y423.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X34.500000 Y417.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X36.000000 Y427.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X39.500000 Y432.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.666667 Y426.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X49.000000 Y424.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X45.500000 Y418.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X52.833333 Y412.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X60.500000 Y406.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X63.000000 Y397.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.833333 Y402.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X77.500000 Y409.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X80.333333 Y417.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X86.833333 Y416.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X92.000000 Y423.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X86.500000 Y427.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X81.666667 Y433.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X76.833333 Y425.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X73.000000 Y419.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X66.833333 Y421.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X67.666667 Y412.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X58.666667 Y424.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X53.166667 Y431.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X52.666667 Y450.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X58.666667 Y455.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X62.333333 Y461.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X65.833333 Y467.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.833333 Y471.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X65.333333 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X58.833333 Y472.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X52.333333 Y472.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X44.666667 Y472.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X41.666667 Y465.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X38.666667 Y459.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.833333 Y453.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.333333 Y459.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X55.000000 Y464.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X65.833333 Y456.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X72.833333 Y458.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X79.500000 Y458.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X85.166667 Y462.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X82.500000 Y469.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X86.000000 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X76.666667 Y466.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X75.833333 Y449.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X63.833333 Y447.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X91.500000 Y438.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X95.666667 Y433.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X97.333333 Y441.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X98.666667 Y447.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X107.500000 Y450.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X109.000000 Y456.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X111.000000 Y463.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X106.333333 Y442.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X104.166667 Y435.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X108.000000 Y426.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X101.666667 Y425.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X101.833333 Y418.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X96.500000 Y414.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X99.000000 Y408.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X93.166667 Y398.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X101.166667 Y393.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X94.666667 Y386.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X86.333333 Y390.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X77.666667 Y396.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X85.000000 Y403.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.000000 Y387.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X60.666667 Y385.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X54.166667 Y393.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X44.833333 Y391.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.833333 Y382.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X56.333333 Y377.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X55.166667 Y367.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X54.666667 Y358.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X63.500000 Y361.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X64.000000 Y370.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.000000 Y378.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X78.333333 Y382.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X79.000000 Y371.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X72.000000 Y365.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X79.500000 Y359.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X87.500000 Y364.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X87.500000 Y354.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X79.833333 Y349.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X87.833333 Y342.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X95.000000 Y348.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X103.166667 Y353.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X97.500000 Y361.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X95.333333 Y370.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X100.500000 Y378.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X109.500000 Y384.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X113.000000 Y375.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X118.833333 Y368.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X126.833333 Y363.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X123.500000 Y354.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X122.833333 Y345.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X117.500000 Y337.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X108.500000 Y333.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X108.166667 Y342.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X114.500000 Y351.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X112.333333 Y361.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X106.666667 Y368.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X120.166667 Y384.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X128.000000 Y389.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X134.500000 Y382.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X141.166667 Y376.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X133.500000 Y370.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X126.333333 Y376.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X138.000000 Y359.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X146.333333 Y364.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X151.500000 Y372.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X159.500000 Y377.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X162.500000 Y367.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X155.666667 Y361.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X151.000000 Y352.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X142.333333 Y348.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X137.000000 Y340.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X146.166667 Y337.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X154.833333 Y341.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X156.333333 Y332.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X163.833333 Y338.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X164.000000 Y347.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X166.833333 Y356.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X174.666667 Y363.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X171.666667 Y372.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X183.666667 Y375.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X187.000000 Y370.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X193.000000 Y367.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X198.500000 Y370.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X192.166667 Y377.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X197.166667 Y385.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X190.000000 Y390.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X198.333333 Y399.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X204.833333 Y392.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X210.166667 Y384.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X205.333333 Y376.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X213.666667 Y370.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X220.000000 Y377.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X226.666667 Y384.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X218.500000 Y389.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X212.833333 Y397.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X208.166667 Y405.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X214.333333 Y412.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X220.833333 Y405.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X226.000000 Y398.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X234.333333 Y391.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X237.833333 Y382.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X245.833333 Y377.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X250.833333 Y370.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X246.166667 Y363.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X244.000000 Y354.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X252.333333 Y356.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X261.000000 Y354.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.833333 Y363.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.333333 Y368.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X269.833333 Y361.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X273.833333 Y353.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X268.000000 Y346.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X277.166667 Y343.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X284.500000 Y349.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X290.666667 Y342.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X297.833333 Y337.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X298.333333 Y346.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X304.666667 Y352.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X300.833333 Y359.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X293.833333 Y354.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.166667 Y361.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X280.666667 Y359.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X276.500000 Y367.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X284.333333 Y371.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X288.500000 Y379.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.333333 Y380.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X272.000000 Y375.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.333333 Y381.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.500000 Y375.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X253.666667 Y383.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X245.666667 Y387.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X242.166667 Y396.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.166667 Y403.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X240.166667 Y411.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X233.333333 Y418.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X240.500000 Y424.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X249.666667 Y417.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X257.666667 Y411.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X260.333333 Y420.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.166667 Y428.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X275.833333 Y425.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X280.166667 Y416.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X270.833333 Y415.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X266.000000 Y407.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X269.500000 Y398.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X275.333333 Y406.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X285.166667 Y408.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X294.000000 Y405.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X303.000000 Y403.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X312.000000 Y405.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X315.166667 Y414.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X317.666667 Y423.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X323.166667 Y427.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X327.000000 Y433.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X333.166667 Y429.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X336.000000 Y436.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X335.833333 Y442.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.500000 Y440.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X323.166667 Y442.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X320.166667 Y448.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X314.500000 Y451.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X309.500000 Y456.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X305.666667 Y450.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X299.666667 Y448.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X293.166667 Y447.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.166667 Y452.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X286.166667 Y459.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.166667 Y464.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X285.166667 Y471.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X293.000000 Y472.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X294.000000 Y464.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X303.000000 Y459.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X296.333333 Y453.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X295.833333 Y441.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.166667 Y437.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X294.000000 Y432.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X300.500000 Y432.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X305.833333 Y436.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X307.333333 Y442.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X313.666667 Y444.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X317.500000 Y439.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X315.000000 Y431.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X309.000000 Y428.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X307.000000 Y419.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X301.166667 Y412.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X292.166667 Y415.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X297.833333 Y422.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X286.833333 Y423.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.500000 Y432.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.500000 Y438.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X276.166667 Y437.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X270.166667 Y439.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X276.833333 Y445.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X280.333333 Y452.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X273.166667 Y453.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X275.833333 Y459.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X271.166667 Y469.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X276.000000 Y474.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X263.833333 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X301.833333 Y472.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X309.333333 Y468.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X313.500000 Y462.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X319.166667 Y458.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X319.500000 Y467.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X327.000000 Y463.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.500000 Y457.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X331.666667 Y448.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X338.666667 Y449.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X345.333333 Y449.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X342.166667 Y457.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X339.666667 Y463.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X334.166667 Y467.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X339.166667 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.666667 Y469.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X349.666667 Y463.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X350.500000 Y457.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X354.000000 Y449.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X354.166667 Y442.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X354.000000 Y436.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X350.166667 Y430.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.666667 Y424.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.833333 Y417.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X351.666667 Y413.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X356.833333 Y408.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X363.666667 Y411.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X370.000000 Y409.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X369.833333 Y402.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X363.333333 Y402.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X369.333333 Y395.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X362.166667 Y393.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X364.333333 Y387.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X358.000000 Y385.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X352.166667 Y388.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X355.666667 Y394.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X349.166667 Y394.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X352.666667 Y399.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.333333 Y406.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X341.000000 Y402.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X334.500000 Y402.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X334.666667 Y409.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X339.666667 Y413.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X330.000000 Y413.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X331.666667 Y420.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X323.666667 Y419.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X323.666667 Y407.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X321.000000 Y398.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X317.500000 Y389.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X310.500000 Y396.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X305.833333 Y388.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X308.166667 Y378.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X299.166667 Y376.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X293.166667 Y369.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X304.333333 Y368.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X313.166667 Y370.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X319.666667 Y364.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X311.500000 Y359.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X313.833333 Y349.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X307.000000 Y343.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X312.166667 Y336.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X314.833333 Y328.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X323.000000 Y326.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X331.333333 Y327.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.500000 Y318.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X337.666667 Y320.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X341.666667 Y315.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X345.333333 Y310.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X340.000000 Y306.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X334.333333 Y309.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X325.166667 Y310.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X316.833333 Y307.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X310.333333 Y309.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X311.666667 Y316.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X308.333333 Y322.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X304.000000 Y330.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X296.166667 Y321.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.833333 Y329.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.000000 Y334.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X271.000000 Y333.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X275.666667 Y325.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X283.000000 Y319.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.333333 Y312.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X287.500000 Y303.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X284.333333 Y297.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X277.833333 Y297.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X273.000000 Y301.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X266.833333 Y295.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X260.500000 Y301.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X253.500000 Y307.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.000000 Y315.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.166667 Y309.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X272.000000 Y315.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X278.000000 Y309.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.833333 Y323.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X256.000000 Y329.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X250.833333 Y321.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X240.500000 Y318.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X244.833333 Y310.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X243.500000 Y301.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X235.666667 Y298.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X235.833333 Y306.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X229.833333 Y313.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X220.500000 Y311.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X226.333333 Y304.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X227.000000 Y295.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X218.333333 Y297.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X212.000000 Y303.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X206.666667 Y295.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X201.500000 Y287.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X207.666667 Y281.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X208.333333 Y271.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X217.333333 Y274.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X215.500000 Y265.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X224.500000 Y268.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X222.333333 Y258.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X226.166667 Y249.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X232.166667 Y241.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X241.000000 Y239.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X244.000000 Y248.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X252.166667 Y252.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X252.333333 Y261.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X243.000000 Y259.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.333333 Y253.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X233.166667 Y262.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X235.500000 Y271.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X232.000000 Y277.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X241.333333 Y277.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X243.333333 Y271.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X249.166667 Y274.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X249.000000 Y283.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X256.833333 Y280.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.666667 Y288.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.000000 Y281.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.166667 Y275.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X270.500000 Y271.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X267.500000 Y265.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X275.500000 Y261.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X271.500000 Y252.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X277.000000 Y245.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X275.166667 Y236.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X269.000000 Y230.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X261.333333 Y234.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X268.000000 Y242.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X259.500000 Y245.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X250.333333 Y238.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X245.166667 Y230.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X254.166667 Y225.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X246.666667 Y220.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X239.500000 Y215.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X232.833333 Y218.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.666667 Y223.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X230.166667 Y224.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X233.666667 Y231.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X224.666667 Y230.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X220.833333 Y238.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X210.833333 Y241.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X206.666667 Y232.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X215.333333 Y228.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X220.000000 Y221.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X214.833333 Y217.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X209.333333 Y214.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X202.333333 Y217.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X196.000000 Y219.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X198.666667 Y227.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X206.666667 Y222.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X202.833333 Y206.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X202.500000 Y196.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X195.000000 Y190.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X188.000000 Y184.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X183.166667 Y176.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X192.333333 Y172.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X198.500000 Y179.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X207.333333 Y185.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X212.166667 Y177.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X206.166667 Y170.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X216.333333 Y167.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X225.333333 Y169.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X221.333333 Y179.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X227.000000 Y187.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X237.666667 Y188.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X241.333333 Y198.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X231.333333 Y198.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X234.000000 Y207.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X224.833333 Y208.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X219.000000 Y211.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X224.000000 Y215.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X221.833333 Y199.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X211.666667 Y200.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X215.833333 Y189.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X234.333333 Y177.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X243.833333 Y179.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X240.500000 Y170.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X233.833333 Y162.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.333333 Y153.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X245.500000 Y158.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X251.000000 Y166.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X261.166667 Y168.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X271.166667 Y170.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X266.000000 Y178.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X259.000000 Y187.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X249.500000 Y189.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X255.833333 Y199.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X266.000000 Y196.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X273.500000 Y188.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.500000 Y179.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X284.333333 Y171.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X293.333333 Y168.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X300.500000 Y162.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X307.333333 Y154.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X315.833333 Y151.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X311.666667 Y143.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X318.000000 Y137.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X322.333333 Y144.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X330.000000 Y153.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X335.500000 Y145.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X341.833333 Y146.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X344.333333 Y140.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X338.000000 Y138.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X330.166667 Y135.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X333.000000 Y129.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.166667 Y124.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X325.000000 Y119.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X321.000000 Y113.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X317.166667 Y121.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X310.333333 Y126.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X306.833333 Y135.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X298.000000 Y138.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.333333 Y141.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X288.333333 Y132.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.666667 Y126.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X277.666667 Y130.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X271.166667 Y130.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X268.000000 Y124.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X264.500000 Y119.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X261.500000 Y113.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X256.500000 Y106.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X251.333333 Y102.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X244.333333 Y98.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X243.666667 Y105.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X237.166667 Y107.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X232.333333 Y102.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X225.833333 Y102.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X221.166667 Y97.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X217.000000 Y89.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X223.333333 Y88.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X228.500000 Y84.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X231.166667 Y78.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X230.500000 Y71.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X223.666667 Y73.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X225.500000 Y65.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X219.000000 Y66.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X213.000000 Y62.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X208.833333 Y67.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X202.166667 Y65.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X196.333333 Y68.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X193.333333 Y74.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X199.833333 Y74.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X206.500000 Y79.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X215.000000 Y76.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X202.333333 Y87.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X194.000000 Y81.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X190.166667 Y86.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X185.166667 Y91.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X184.500000 Y97.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X191.000000 Y97.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X193.166667 Y103.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X190.333333 Y109.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X197.000000 Y111.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X194.500000 Y117.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X198.333333 Y123.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X194.833333 Y129.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X193.166667 Y135.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X188.333333 Y131.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X182.833333 Y134.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X179.000000 Y129.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.833333 Y132.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X168.333333 Y127.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X173.333333 Y123.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X170.000000 Y117.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X174.166667 Y112.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.333333 Y106.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X166.166667 Y102.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X159.833333 Y103.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X162.666667 Y109.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X157.500000 Y115.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X153.500000 Y120.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X153.833333 Y126.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X152.166667 Y133.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X151.166667 Y140.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.666667 Y146.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X150.166667 Y152.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X151.166667 Y160.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X156.166667 Y164.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X152.500000 Y170.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.833333 Y174.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X153.500000 Y178.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X154.333333 Y185.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X158.500000 Y192.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X163.833333 Y188.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X167.833333 Y195.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X175.833333 Y199.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X184.833333 Y195.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X192.333333 Y202.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X192.500000 Y211.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X184.833333 Y215.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X184.166667 Y207.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X176.333333 Y210.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X177.000000 Y220.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X168.333333 Y219.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X164.333333 Y214.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X159.500000 Y219.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X155.666667 Y214.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X156.000000 Y206.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X153.833333 Y200.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.500000 Y201.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.833333 Y208.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.500000 Y216.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X142.500000 Y220.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X145.666667 Y228.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X149.000000 Y234.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X152.000000 Y240.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X149.000000 Y246.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X155.833333 Y249.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X154.500000 Y255.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.000000 Y256.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X167.833333 Y260.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X170.666667 Y267.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X175.333333 Y263.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X179.666667 Y271.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.000000 Y274.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X173.166667 Y281.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X168.166667 Y285.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X174.333333 Y287.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.166667 Y294.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X176.833333 Y298.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X174.166667 Y304.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X180.333333 Y307.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X187.500000 Y303.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X186.833333 Y293.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X180.833333 Y286.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X185.333333 Y278.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X191.666667 Y285.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X196.000000 Y295.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X201.166667 Y303.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X196.000000 Y311.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X191.833333 Y320.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X186.000000 Y323.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X187.500000 Y329.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X184.833333 Y335.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X190.500000 Y339.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X187.666667 Y345.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X193.000000 Y349.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X199.333333 Y347.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X202.833333 Y339.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X206.833333 Y330.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X215.833333 Y332.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X225.000000 Y334.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X234.333333 Y334.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X240.833333 Y328.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X230.833333 Y324.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X221.166667 Y322.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X212.000000 Y320.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X206.000000 Y313.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X200.833333 Y323.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X195.833333 Y331.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X179.833333 Y321.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X184.166667 Y316.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X169.666667 Y315.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.000000 Y311.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X156.500000 Y320.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X149.000000 Y314.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X142.000000 Y307.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X138.500000 Y298.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X130.833333 Y292.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X140.500000 Y288.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X134.000000 Y281.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X139.000000 Y273.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X136.166667 Y264.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X129.500000 Y271.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X122.166667 Y262.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X118.166667 Y253.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X122.500000 Y245.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X131.000000 Y240.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X136.333333 Y248.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X142.000000 Y255.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X146.500000 Y264.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X151.000000 Y272.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.500000 Y281.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X156.500000 Y284.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.333333 Y294.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X164.833333 Y303.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X153.000000 Y303.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X148.666667 Y293.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X129.500000 Y304.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X132.000000 Y313.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X129.333333 Y322.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X136.666667 Y330.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X127.000000 Y334.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X118.166667 Y326.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X113.833333 Y318.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X105.333333 Y314.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X96.833333 Y318.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X103.166667 Y325.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X94.833333 Y330.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X86.000000 Y333.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X77.666667 Y338.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.666667 Y329.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X78.833333 Y324.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X87.500000 Y321.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X84.000000 Y310.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X93.000000 Y307.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X101.833333 Y303.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X110.500000 Y299.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X115.333333 Y291.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X120.666667 Y299.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X114.500000 Y307.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X122.000000 Y313.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X140.500000 Y319.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.833333 Y327.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X165.333333 Y326.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X173.666667 Y331.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X174.500000 Y341.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X177.000000 Y350.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X184.333333 Y356.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X194.166667 Y357.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X200.666667 Y356.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X207.166667 Y363.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X218.000000 Y359.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X222.833333 Y352.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X220.000000 Y345.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X212.000000 Y342.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X210.333333 Y350.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X228.500000 Y345.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X233.666667 Y352.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X228.166667 Y359.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.666667 Y361.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X240.833333 Y369.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X233.000000 Y374.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X226.166667 Y367.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X241.333333 Y343.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X249.333333 Y336.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X260.166667 Y338.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X252.833333 Y347.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X261.166667 Y391.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X259.333333 Y400.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X252.166667 Y394.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X249.833333 Y405.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X229.333333 Y409.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X223.000000 Y416.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X214.000000 Y425.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X206.666667 Y420.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X197.166667 Y432.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X191.000000 Y414.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X224.833333 Y447.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X219.166667 Y454.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X252.833333 Y426.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X287.166667 Y444.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X313.666667 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X325.166667 Y474.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X355.500000 Y466.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X359.666667 Y461.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X364.666667 Y465.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X369.333333 Y460.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X377.833333 Y460.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X381.833333 Y467.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X392.500000 Y469.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X401.333333 Y469.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.833333 Y464.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X411.333333 Y472.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X418.666667 Y472.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X420.333333 Y462.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X414.333333 Y457.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.166667 Y455.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X412.166667 Y449.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X405.666667 Y448.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X399.166667 Y447.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X392.666667 Y449.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X394.833333 Y455.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X399.333333 Y460.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X390.500000 Y460.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.333333 Y452.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X379.000000 Y451.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X372.500000 Y451.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X365.666667 Y451.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X360.166667 Y455.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X361.000000 Y445.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X370.333333 Y444.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X377.166667 Y440.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X383.666667 Y439.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X383.833333 Y445.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X390.666667 Y440.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X397.666667 Y439.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X404.500000 Y441.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.000000 Y435.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X407.666667 Y428.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X415.500000 Y428.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X422.500000 Y434.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X429.666667 Y439.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X423.000000 Y444.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X420.666667 Y452.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X426.000000 Y456.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X431.166667 Y462.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X437.500000 Y464.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X441.000000 Y472.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X433.333333 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X429.666667 Y469.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X440.000000 Y453.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X448.666667 Y454.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X448.500000 Y461.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X448.500000 Y469.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X456.333333 Y467.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X458.166667 Y461.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X458.666667 Y454.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X455.166667 Y449.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X459.666667 Y444.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X460.333333 Y437.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X465.833333 Y441.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X471.666667 Y438.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X470.000000 Y429.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.666667 Y424.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X469.000000 Y420.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X464.000000 Y424.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.000000 Y418.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X453.333333 Y415.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X450.500000 Y407.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X454.333333 Y399.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X448.666667 Y392.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X442.833333 Y399.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X440.666667 Y408.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X434.833333 Y415.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X434.666667 Y421.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X428.500000 Y417.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X423.500000 Y413.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X420.166667 Y407.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X414.666667 Y404.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X409.833333 Y398.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X406.333333 Y403.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X401.333333 Y395.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X405.666667 Y390.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X411.666667 Y385.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X403.166667 Y382.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X396.666667 Y382.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X390.333333 Y384.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X386.333333 Y389.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X380.500000 Y392.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X375.000000 Y388.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X377.500000 Y382.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X381.333333 Y375.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.500000 Y370.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.166667 Y363.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X391.666667 Y364.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X398.000000 Y366.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X394.000000 Y371.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X387.333333 Y378.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X395.000000 Y389.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X392.333333 Y395.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.333333 Y398.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.666667 Y405.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X378.333333 Y406.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X377.333333 Y399.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X378.000000 Y414.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X377.500000 Y421.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X377.166667 Y427.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X370.166667 Y428.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X369.666667 Y435.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X362.666667 Y435.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X357.500000 Y429.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X363.333333 Y426.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X364.333333 Y419.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X370.500000 Y421.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X358.000000 Y417.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X355.666667 Y423.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X339.500000 Y424.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X342.500000 Y434.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.000000 Y440.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X362.833333 Y474.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X371.166667 Y474.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X384.666667 Y430.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X392.166667 Y432.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X399.000000 Y426.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X394.000000 Y418.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X396.500000 Y410.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X394.666667 Y404.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X387.166667 Y413.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.833333 Y421.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X402.666667 Y417.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X407.666667 Y412.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X415.666667 Y412.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X412.833333 Y419.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X420.333333 Y421.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X428.166667 Y424.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X433.166667 Y429.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X438.000000 Y434.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X441.000000 Y442.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X449.000000 Y444.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X449.333333 Y437.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X454.166667 Y433.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X448.333333 Y429.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X442.500000 Y426.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X445.000000 Y418.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X454.500000 Y424.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X460.166667 Y429.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X471.666667 Y448.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X466.333333 Y452.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.833333 Y458.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X469.666667 Y462.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X465.666667 Y467.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X470.833333 Y471.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X465.333333 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X458.500000 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X432.166667 Y449.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X414.833333 Y443.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X430.666667 Y407.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X433.166667 Y399.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X424.500000 Y398.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X428.333333 Y390.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X419.500000 Y390.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X422.833333 Y380.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X421.166667 Y373.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X415.333333 Y370.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.833333 Y371.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X404.166667 Y376.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X414.666667 Y377.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X428.166667 Y375.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X432.166667 Y382.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X438.166667 Y377.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X444.166667 Y383.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X448.333333 Y378.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X455.833333 Y379.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X462.333333 Y379.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X458.500000 Y387.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X468.666667 Y385.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X473.833333 Y380.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.166667 Y369.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X467.500000 Y362.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X462.833333 Y370.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X454.166667 Y362.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X460.166667 Y355.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X454.500000 Y347.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X449.166667 Y354.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X440.833333 Y359.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X431.500000 Y356.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X427.666667 Y348.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X433.500000 Y340.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X439.333333 Y348.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X446.333333 Y341.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X440.333333 Y333.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X431.666667 Y330.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X423.333333 Y334.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X414.333333 Y331.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X410.666667 Y340.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X418.666667 Y345.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X411.500000 Y351.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.833333 Y360.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X402.000000 Y354.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X402.166667 Y344.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X393.666667 Y340.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X391.166667 Y349.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X383.333333 Y355.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X378.000000 Y363.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X371.833333 Y368.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X369.666667 Y362.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X365.333333 Y357.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X358.833333 Y357.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X351.666667 Y361.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X351.000000 Y355.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X344.833333 Y353.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X345.500000 Y346.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.833333 Y340.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X343.000000 Y334.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X336.666667 Y336.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X338.666667 Y345.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.500000 Y344.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X323.166667 Y350.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X319.166667 Y341.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X324.333333 Y335.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X320.000000 Y318.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X319.833333 Y301.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X323.000000 Y295.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X325.666667 Y289.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X319.333333 Y284.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X326.500000 Y279.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X333.666667 Y284.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X342.500000 Y283.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X348.833333 Y284.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X358.500000 Y283.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X356.500000 Y292.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X366.000000 Y292.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X368.500000 Y282.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X376.500000 Y288.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.333333 Y291.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X391.333333 Y299.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X400.000000 Y297.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.333333 Y301.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X400.500000 Y307.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.666667 Y314.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X410.500000 Y321.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X406.500000 Y326.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X400.666667 Y333.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X392.500000 Y328.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X386.833333 Y321.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X378.333333 Y325.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X384.000000 Y333.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X373.166667 Y338.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X380.166667 Y344.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X372.166667 Y350.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X365.166667 Y343.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X356.166667 Y346.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X356.500000 Y335.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X350.500000 Y327.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X341.666667 Y326.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X352.666667 Y318.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X360.333333 Y312.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X355.000000 Y304.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X364.000000 Y301.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X372.833333 Y307.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X374.666667 Y298.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X382.500000 Y306.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X390.333333 Y311.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X397.666667 Y318.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X416.666667 Y318.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X423.666667 Y324.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X424.833333 Y313.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X417.500000 Y307.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X428.166667 Y302.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X431.500000 Y292.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X439.166667 Y287.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X447.166667 Y282.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X440.833333 Y274.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X432.833333 Y269.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X432.333333 Y279.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X425.166667 Y285.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X415.833333 Y283.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X406.333333 Y282.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X398.666667 Y277.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X396.000000 Y286.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.000000 Y281.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X377.500000 Y275.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X380.000000 Y266.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X376.000000 Y257.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X386.000000 Y256.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X393.000000 Y248.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X399.833333 Y254.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X395.333333 Y263.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X390.666667 Y271.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X404.333333 Y266.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X412.666667 Y272.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X422.166667 Y273.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X420.833333 Y264.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X412.833333 Y259.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X410.000000 Y248.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X403.000000 Y242.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X398.333333 Y233.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X390.333333 Y238.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X390.333333 Y227.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X390.833333 Y218.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X383.166667 Y212.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X388.500000 Y208.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X394.666667 Y203.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X389.000000 Y195.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X382.666667 Y191.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X378.833333 Y185.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X378.500000 Y179.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X372.833333 Y182.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X377.333333 Y172.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X371.000000 Y171.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X375.833333 Y165.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X382.000000 Y162.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X389.000000 Y156.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X381.166667 Y152.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X373.166667 Y156.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X363.000000 Y154.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X357.500000 Y150.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X350.833333 Y148.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X345.166667 Y154.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X339.166667 Y159.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X331.166667 Y163.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X338.500000 Y171.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X345.666667 Y168.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X350.666667 Y162.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X355.833333 Y166.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X357.333333 Y160.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X364.166667 Y164.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X362.666667 Y175.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X356.166667 Y176.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X355.333333 Y183.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X352.833333 Y189.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X350.833333 Y195.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X351.500000 Y202.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X356.500000 Y206.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X360.333333 Y198.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X366.166667 Y201.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X368.666667 Y208.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X373.666667 Y215.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X367.833333 Y218.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X364.000000 Y224.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X365.000000 Y230.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X371.500000 Y229.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X380.333333 Y233.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X379.000000 Y223.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X372.000000 Y239.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X363.333333 Y237.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X357.833333 Y241.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X362.833333 Y245.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X357.333333 Y248.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X355.333333 Y255.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X349.000000 Y256.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.833333 Y262.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X345.166667 Y269.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X343.500000 Y275.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X337.000000 Y273.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.833333 Y268.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X321.000000 Y271.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X322.333333 Y261.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.666667 Y255.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X338.833333 Y253.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X344.333333 Y247.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X350.166667 Y244.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X340.333333 Y236.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.333333 Y230.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X351.333333 Y223.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X349.166667 Y215.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.833333 Y207.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X337.500000 Y209.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X330.500000 Y203.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X324.333333 Y210.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X330.833333 Y215.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X339.500000 Y218.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X335.666667 Y226.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.333333 Y233.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X322.000000 Y238.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X313.000000 Y241.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X308.500000 Y232.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X301.333333 Y226.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X303.000000 Y218.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X300.833333 Y212.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X296.000000 Y219.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.500000 Y218.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X284.666667 Y214.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X290.166667 Y210.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X286.666667 Y205.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X292.500000 Y198.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X302.000000 Y202.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X310.166667 Y204.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X318.166667 Y200.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X319.666667 Y190.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X325.833333 Y183.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X331.333333 Y176.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X322.000000 Y170.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X316.000000 Y177.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X309.833333 Y184.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X308.166667 Y195.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X298.833333 Y188.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X299.833333 Y178.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X306.166667 Y171.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X312.666667 Y164.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X320.166667 Y159.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X303.333333 Y146.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X295.666667 Y151.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X287.833333 Y157.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.333333 Y162.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X275.333333 Y154.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X269.500000 Y161.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.666667 Y156.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X263.500000 Y148.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X272.166667 Y145.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X266.833333 Y137.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X257.333333 Y138.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X261.000000 Y128.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X257.000000 Y120.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X253.500000 Y115.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X248.666667 Y110.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X245.833333 Y118.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X249.166667 Y123.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X247.333333 Y132.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X237.666667 Y127.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X240.000000 Y121.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X234.500000 Y117.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X227.333333 Y112.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X219.666667 Y117.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X213.333333 Y122.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X208.000000 Y127.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X205.833333 Y121.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X203.166667 Y114.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X209.666667 Y112.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X204.500000 Y106.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X205.666667 Y99.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X212.166667 Y100.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X209.833333 Y94.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X197.500000 Y98.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X180.666667 Y103.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X180.666667 Y111.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X185.166667 Y115.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X179.666667 Y119.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X187.500000 Y123.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X201.666667 Y133.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X207.833333 Y135.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X211.666667 Y140.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X215.833333 Y135.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X221.833333 Y138.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X227.166667 Y131.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X231.166667 Y126.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X225.166667 Y123.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X219.666667 Y129.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X234.333333 Y136.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X241.500000 Y143.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X250.833333 Y147.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X227.500000 Y146.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X216.833333 Y149.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X223.000000 Y157.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X209.333333 Y159.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X204.500000 Y150.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X200.166667 Y141.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X189.500000 Y144.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X185.000000 Y152.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X194.333333 Y153.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X197.833333 Y162.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X188.500000 Y164.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X177.333333 Y167.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X174.666667 Y158.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X168.666667 Y151.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.000000 Y151.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X156.833333 Y145.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X162.166667 Y136.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X159.666667 Y130.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X160.000000 Y121.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.500000 Y125.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X141.500000 Y117.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X131.333333 Y114.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X128.333333 Y105.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X119.833333 Y101.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X115.833333 Y92.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X122.000000 Y85.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X127.000000 Y93.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X136.500000 Y96.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X140.333333 Y106.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X149.666667 Y108.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X149.000000 Y98.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X145.500000 Y88.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X136.166667 Y86.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X131.833333 Y78.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X136.333333 Y68.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X142.166667 Y76.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X151.666667 Y79.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X155.500000 Y88.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X163.166667 Y94.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.000000 Y97.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X177.666667 Y89.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X170.166667 Y84.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X162.000000 Y78.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X157.000000 Y69.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X157.666667 Y59.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X165.666667 Y53.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X160.500000 Y45.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X152.166667 Y50.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X144.500000 Y56.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X139.833333 Y48.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X130.333333 Y47.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X125.666667 Y55.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X134.833333 Y58.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X126.666667 Y67.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X118.833333 Y75.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X109.833333 Y71.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X106.666667 Y60.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X116.166667 Y61.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X114.500000 Y51.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X106.500000 Y44.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X100.666667 Y52.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X93.333333 Y44.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X86.333333 Y51.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X91.333333 Y60.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X97.833333 Y68.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X101.333333 Y79.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X110.000000 Y83.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X102.500000 Y92.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X93.833333 Y88.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X86.666667 Y97.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X95.333333 Y100.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X93.000000 Y110.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X84.000000 Y108.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X84.833333 Y117.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X75.666667 Y116.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.333333 Y109.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X61.166667 Y109.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X63.833333 Y118.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.166667 Y125.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X79.333333 Y125.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X83.333333 Y134.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X74.000000 Y133.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X64.500000 Y132.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X57.500000 Y126.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X53.000000 Y134.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.000000 Y127.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.333333 Y123.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X44.333333 Y117.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X35.833333 Y115.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X34.333333 Y122.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X29.333333 Y116.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X26.000000 Y110.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X31.500000 Y107.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y103.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X28.333333 Y95.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X34.666667 Y97.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X41.166667 Y99.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.833333 Y106.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.833333 Y109.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X54.333333 Y117.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X55.500000 Y99.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X61.166667 Y92.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X65.333333 Y100.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X75.500000 Y101.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X73.833333 Y92.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X82.666667 Y86.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X75.833333 Y77.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X67.833333 Y82.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X62.166667 Y73.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.166667 Y68.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X61.666667 Y64.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X69.166667 Y57.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X78.666667 Y60.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X84.000000 Y69.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X90.333333 Y78.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X107.500000 Y101.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X112.000000 Y110.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X108.500000 Y119.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X99.166667 Y119.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X92.666667 Y126.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X96.166667 Y135.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X104.833333 Y138.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X112.666667 Y133.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X105.333333 Y127.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X117.166667 Y122.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X126.333333 Y125.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X135.500000 Y127.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X142.000000 Y135.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X138.500000 Y145.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X131.666667 Y139.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X126.333333 Y146.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X132.166667 Y155.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X134.500000 Y164.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X138.333333 Y173.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X139.500000 Y183.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X130.166667 Y182.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X121.166667 Y184.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X116.666667 Y192.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X123.666667 Y199.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X127.166667 Y191.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X137.333333 Y195.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X140.333333 Y205.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X131.000000 Y205.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X132.666667 Y214.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X123.500000 Y213.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X119.333333 Y221.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X110.666667 Y225.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X106.666667 Y233.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X115.833333 Y235.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X124.166667 Y231.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X128.666667 Y223.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X135.166667 Y229.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X140.833333 Y237.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X151.666667 Y224.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X160.666667 Y230.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X166.000000 Y234.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X170.500000 Y229.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X165.166667 Y225.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X179.500000 Y229.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X188.166667 Y233.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X195.833333 Y239.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X202.500000 Y245.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X210.000000 Y254.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X216.833333 Y248.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X200.833333 Y262.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X195.000000 Y255.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X188.333333 Y248.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X180.833333 Y242.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.833333 Y238.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X167.166667 Y241.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X160.833333 Y240.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X167.000000 Y250.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.500000 Y247.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X178.166667 Y254.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X185.500000 Y261.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X191.000000 Y269.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X197.666667 Y275.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X216.166667 Y288.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X224.500000 Y283.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X234.000000 Y286.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X242.666667 Y292.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X250.833333 Y296.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X273.000000 Y285.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X273.833333 Y277.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.666667 Y271.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X288.000000 Y270.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X286.000000 Y261.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X294.666667 Y263.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X301.000000 Y271.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X310.166667 Y277.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X304.833333 Y284.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X298.500000 Y292.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X294.333333 Y297.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X296.666667 Y303.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X305.000000 Y298.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X314.000000 Y291.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.166667 Y301.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X333.500000 Y293.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X341.166667 Y293.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X347.333333 Y296.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X351.833333 Y273.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X362.333333 Y274.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X370.000000 Y266.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X365.166667 Y257.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X369.166667 Y248.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X380.833333 Y245.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X355.833333 Y230.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X360.000000 Y215.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X364.833333 Y193.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X362.666667 Y187.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X373.500000 Y193.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X378.833333 Y201.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.833333 Y184.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X393.833333 Y178.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X396.500000 Y187.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X404.166667 Y196.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X413.166667 Y198.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X422.500000 Y200.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X432.166667 Y201.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X429.666667 Y210.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X426.166667 Y218.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X417.666667 Y222.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X410.500000 Y216.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.000000 Y207.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X400.833333 Y213.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X401.000000 Y222.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X410.000000 Y227.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X413.833333 Y236.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X420.500000 Y243.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X423.000000 Y252.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X430.666667 Y258.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X439.000000 Y253.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X432.833333 Y246.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X428.166667 Y237.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X423.500000 Y229.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X432.166667 Y225.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X439.333333 Y223.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X445.666667 Y221.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X446.333333 Y215.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X452.833333 Y215.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.333333 Y211.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X464.500000 Y219.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X456.333333 Y224.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X458.333333 Y233.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X451.333333 Y240.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X442.000000 Y242.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X447.833333 Y249.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X457.500000 Y250.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X464.333333 Y244.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X472.333333 Y248.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.666667 Y258.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X465.166667 Y256.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X458.833333 Y266.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X462.666667 Y275.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X468.666667 Y268.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.833333 Y276.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X470.333333 Y284.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X463.833333 Y292.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X457.833333 Y300.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X450.166667 Y305.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X444.666667 Y313.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X445.500000 Y323.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X451.000000 Y330.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X459.166667 Y335.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X468.333333 Y333.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.833333 Y326.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X472.833333 Y324.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X468.166667 Y316.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.833333 Y309.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X467.166667 Y301.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.166667 Y308.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X455.500000 Y316.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X434.833333 Y318.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X436.333333 Y307.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X442.666667 Y297.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X451.166667 Y293.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X456.833333 Y282.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X450.333333 Y272.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X442.666667 Y264.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X450.500000 Y259.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X437.333333 Y233.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X445.833333 Y229.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X436.333333 Y217.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X440.333333 Y212.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X441.666667 Y202.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X449.666667 Y194.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X455.333333 Y203.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X463.333333 Y194.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X469.500000 Y202.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X473.833333 Y212.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X473.666667 Y222.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X468.666667 Y230.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X472.666667 Y239.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X473.666667 Y191.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X468.333333 Y181.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X460.833333 Y174.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.333333 Y165.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X452.666667 Y160.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X443.000000 Y163.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X444.833333 Y153.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X435.833333 Y150.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X435.666667 Y141.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X439.500000 Y132.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X430.333333 Y133.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X431.833333 Y124.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X436.000000 Y116.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X441.666667 Y123.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X451.166667 Y125.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X459.500000 Y129.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X465.166667 Y122.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X456.833333 Y118.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X448.500000 Y114.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X442.166667 Y106.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X435.333333 Y99.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X430.000000 Y106.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X425.333333 Y115.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X417.166667 Y119.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X409.333333 Y124.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X401.666667 Y129.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X392.666667 Y127.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X398.833333 Y120.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X393.000000 Y113.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X383.333333 Y107.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X378.000000 Y115.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X368.666667 Y115.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X360.666667 Y110.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X353.333333 Y114.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X356.000000 Y120.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X356.500000 Y126.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X357.000000 Y133.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X361.500000 Y138.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X356.666667 Y142.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X351.000000 Y139.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X349.166667 Y131.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X350.500000 Y123.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X347.000000 Y117.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X341.833333 Y124.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X341.333333 Y130.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X338.000000 Y118.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X333.500000 Y113.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.000000 Y108.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X336.000000 Y106.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X332.666667 Y101.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X332.833333 Y94.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X326.833333 Y91.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X326.166667 Y98.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X320.666667 Y103.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X314.666667 Y106.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X307.333333 Y107.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X310.500000 Y114.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X303.166667 Y120.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X300.833333 Y112.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X296.666667 Y107.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X291.500000 Y103.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X288.666667 Y97.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X285.666667 Y91.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X291.666667 Y89.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X299.166667 Y91.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X297.000000 Y98.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X303.333333 Y100.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X310.333333 Y98.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X315.333333 Y94.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X307.833333 Y90.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X308.333333 Y84.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X314.833333 Y84.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X321.500000 Y87.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X324.833333 Y82.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X330.833333 Y84.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X334.333333 Y79.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X337.000000 Y87.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X343.333333 Y88.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X342.166667 Y95.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X345.666667 Y100.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X349.500000 Y108.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X342.166667 Y109.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X360.500000 Y100.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X354.166667 Y92.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X358.166667 Y84.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X361.500000 Y74.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X352.333333 Y70.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X354.833333 Y60.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X345.166667 Y61.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X347.000000 Y51.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X337.500000 Y51.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X328.000000 Y51.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X330.166667 Y42.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X320.833333 Y44.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X314.666667 Y36.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X305.500000 Y34.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X308.333333 Y25.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X298.833333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.166667 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X292.666667 Y34.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X283.166667 Y33.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.500000 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X273.666667 Y32.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X267.500000 Y25.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.000000 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X262.333333 Y35.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.833333 Y44.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X248.666667 Y44.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X251.666667 Y35.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X240.833333 Y35.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.000000 Y45.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X224.833333 Y44.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X219.833333 Y35.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X214.000000 Y43.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X209.333333 Y34.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X214.333333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X224.000000 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X229.833333 Y35.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.166667 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X245.833333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X251.333333 Y54.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.833333 Y58.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X263.833333 Y53.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X272.333333 Y51.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X272.000000 Y58.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X277.833333 Y55.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X278.333333 Y47.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X286.500000 Y42.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X295.500000 Y50.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X305.000000 Y53.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X309.000000 Y62.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X309.500000 Y68.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X308.666667 Y76.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X302.833333 Y73.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X302.833333 Y80.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X295.166667 Y79.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X295.000000 Y72.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X293.666667 Y65.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X299.833333 Y63.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X286.166667 Y57.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X280.833333 Y63.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.166667 Y72.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X285.666667 Y72.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X281.333333 Y80.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.333333 Y87.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.500000 Y94.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X273.000000 Y93.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X269.000000 Y88.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.166667 Y93.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X263.666667 Y99.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X270.666667 Y99.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X268.000000 Y106.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X270.500000 Y112.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X276.666667 Y114.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.333333 Y120.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X286.833333 Y118.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.666667 Y111.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.333333 Y111.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X276.000000 Y104.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.666667 Y100.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X294.666667 Y117.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X292.000000 Y124.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X299.666667 Y128.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X280.333333 Y136.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.166667 Y147.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X271.666667 Y118.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X256.833333 Y95.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X251.000000 Y92.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X245.833333 Y87.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X244.500000 Y80.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X238.000000 Y79.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X238.833333 Y85.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.833333 Y94.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X230.666667 Y92.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X218.000000 Y107.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X253.666667 Y83.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X255.333333 Y77.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X250.166667 Y73.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X244.000000 Y70.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X245.333333 Y62.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X251.833333 Y63.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X259.166667 Y71.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X266.833333 Y71.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X272.833333 Y74.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X265.000000 Y78.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X260.000000 Y86.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X267.500000 Y64.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X270.666667 Y41.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X298.833333 Y41.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X308.166667 Y44.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X315.833333 Y53.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X321.166667 Y61.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.166667 Y68.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X334.666667 Y60.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X340.166667 Y69.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X347.333333 Y78.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X369.833333 Y82.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X367.166667 Y92.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X376.333333 Y97.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.833333 Y96.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X388.666667 Y86.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X379.166667 Y86.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X384.500000 Y77.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X389.666667 Y68.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X399.333333 Y63.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X406.500000 Y56.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X416.166667 Y55.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X424.333333 Y60.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X431.000000 Y70.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X441.833333 Y71.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X452.666667 Y72.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.333333 Y78.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X471.166667 Y73.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.500000 Y63.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X467.666667 Y57.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.833333 Y66.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X456.666667 Y55.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.500000 Y47.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X451.500000 Y45.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X446.333333 Y35.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X441.333333 Y27.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X431.666667 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X422.000000 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X418.500000 Y34.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X428.000000 Y36.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X437.500000 Y40.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X442.000000 Y51.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X437.666667 Y60.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X448.333333 Y61.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X447.333333 Y81.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X454.666667 Y87.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X464.000000 Y89.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X459.666667 Y97.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X457.166667 Y106.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X465.333333 Y110.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X471.666667 Y109.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.833333 Y114.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.166667 Y124.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X469.333333 Y132.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.833333 Y142.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X473.666667 Y151.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X470.666667 Y161.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X472.333333 Y170.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.833333 Y154.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X453.500000 Y149.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X445.000000 Y143.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X448.666667 Y134.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X457.000000 Y138.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X464.000000 Y145.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X451.666667 Y171.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X442.166667 Y172.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X433.000000 Y170.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X432.666667 Y159.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X421.000000 Y157.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X414.166667 Y151.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X412.166667 Y141.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X403.333333 Y138.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X404.166667 Y148.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X398.166667 Y155.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X406.833333 Y159.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X412.833333 Y166.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X415.333333 Y176.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X424.666667 Y178.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X435.666667 Y181.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X438.500000 Y191.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X427.833333 Y190.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X418.333333 Y188.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X409.000000 Y186.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X403.666667 Y178.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X401.000000 Y169.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X392.333333 Y165.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X386.500000 Y173.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X395.000000 Y146.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X392.500000 Y137.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X384.500000 Y132.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X376.166667 Y138.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X370.833333 Y145.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.333333 Y143.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X376.166667 Y124.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X368.333333 Y129.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X362.000000 Y122.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X370.666667 Y105.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X385.166667 Y121.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X395.666667 Y102.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X404.833333 Y103.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X411.333333 Y96.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X417.666667 Y89.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X413.333333 Y81.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X405.500000 Y86.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X398.666667 Y93.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X397.000000 Y78.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X405.333333 Y73.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X411.833333 Y66.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X420.000000 Y71.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X424.666667 Y80.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X431.500000 Y89.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X436.500000 Y80.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X443.000000 Y90.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X449.666667 Y98.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X470.500000 Y101.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.833333 Y93.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X473.500000 Y84.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.833333 Y48.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X468.666667 Y38.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X459.000000 Y35.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X462.000000 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X472.500000 Y27.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X451.333333 Y27.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X430.833333 Y50.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X422.500000 Y45.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X411.833333 Y44.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X402.166667 Y45.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X395.333333 Y51.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X387.333333 Y57.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X379.666667 Y63.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X374.166667 Y71.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X366.166667 Y64.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X370.833333 Y55.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X379.500000 Y49.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X388.333333 Y42.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X395.166667 Y35.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X391.666667 Y26.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X401.166667 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X410.833333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X406.666667 Y35.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X381.000000 Y27.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X371.333333 Y27.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X365.333333 Y34.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X360.666667 Y26.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X355.666667 Y34.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X346.166667 Y34.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X341.166667 Y42.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X352.000000 Y43.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X359.500000 Y51.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X367.833333 Y44.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X377.666667 Y37.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X350.666667 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X338.500000 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X328.833333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X333.500000 Y33.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X323.333333 Y32.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X317.833333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X315.500000 Y75.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X322.000000 Y76.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X321.333333 Y129.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X348.166667 Y176.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X342.000000 Y181.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X336.000000 Y187.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X344.500000 Y190.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X340.500000 Y197.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X327.666667 Y195.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X312.333333 Y214.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X317.333333 Y218.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X324.833333 Y222.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X317.500000 Y228.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X310.000000 Y223.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X300.000000 Y235.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X292.833333 Y229.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.666667 Y227.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X280.500000 Y219.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X275.000000 Y223.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X271.500000 Y215.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X264.333333 Y220.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X254.833333 Y214.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X248.166667 Y207.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X262.833333 Y207.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X274.833333 Y204.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X280.666667 Y196.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X287.666667 Y187.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X290.500000 Y178.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X253.666667 Y176.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X284.166667 Y237.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X291.000000 Y243.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X293.500000 Y252.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X283.333333 Y252.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X263.166667 Y256.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X258.833333 Y270.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X282.166667 Y279.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X289.333333 Y288.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X294.333333 Y280.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X311.500000 Y265.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X312.333333 Y254.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X303.333333 Y257.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X304.000000 Y246.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X320.666667 Y250.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X332.833333 Y244.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X337.833333 Y263.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X357.500000 Y263.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X378.000000 Y315.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X368.333333 Y319.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X369.166667 Y328.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X360.166667 Y324.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X335.500000 Y353.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X328.833333 Y360.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X332.333333 Y368.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X324.166667 Y372.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X319.333333 Y380.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X329.000000 Y384.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X330.166667 Y394.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X338.500000 Y386.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X335.166667 Y377.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X345.000000 Y379.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X343.833333 Y369.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X340.833333 Y361.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X353.166667 Y371.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X363.666667 Y365.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X366.500000 Y372.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X372.833333 Y375.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X361.833333 Y379.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X343.000000 Y396.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X297.000000 Y396.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X288.000000 Y398.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X279.000000 Y396.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X284.833333 Y388.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X294.333333 Y387.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X273.000000 Y388.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X302.166667 Y313.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.833333 Y291.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X418.333333 Y295.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X473.833333 Y295.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.833333 Y342.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X465.333333 Y345.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X472.666667 Y354.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X446.166667 Y369.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X432.666667 Y366.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X423.000000 Y365.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X419.833333 Y356.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X437.166667 Y389.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X464.666667 Y396.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X469.333333 Y403.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X461.333333 Y407.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X474.166667 Y411.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X473.000000 Y393.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X418.166667 Y210.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X447.166667 Y182.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X457.500000 Y185.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X423.000000 Y167.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X425.333333 Y146.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X420.833333 Y138.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X422.000000 Y129.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X413.500000 Y132.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X408.166667 Y113.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X417.333333 Y106.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X422.666667 Y97.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X241.166667 Y55.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X232.000000 Y59.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X228.000000 Y54.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X219.666667 Y58.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X210.500000 Y52.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X200.333333 Y54.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X192.833333 Y60.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X182.833333 Y59.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X180.833333 Y68.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X187.000000 Y76.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X183.666667 Y81.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.666667 Y74.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X166.833333 Y67.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X173.000000 Y59.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X174.833333 Y50.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X171.000000 Y41.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X166.333333 Y33.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.166667 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X151.500000 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X142.166667 Y26.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X131.333333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X127.833333 Y35.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X120.500000 Y41.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X113.833333 Y34.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X108.666667 Y26.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X98.333333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X100.166667 Y35.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X90.666667 Y34.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X87.333333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X77.166667 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X67.666667 Y26.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X56.333333 Y26.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X60.666667 Y36.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X71.666667 Y37.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X81.166667 Y36.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X76.000000 Y47.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X64.166667 Y47.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X54.500000 Y54.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X47.166667 Y61.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X36.666667 Y59.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X28.666667 Y67.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X31.166667 Y76.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X37.666667 Y85.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X47.500000 Y80.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X56.333333 Y83.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X47.833333 Y91.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X53.166667 Y70.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X40.666667 Y70.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.166667 Y56.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X32.333333 Y48.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.666667 Y49.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X52.166667 Y44.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.666667 Y35.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X45.333333 Y26.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X35.000000 Y27.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y27.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X29.166667 Y38.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X38.666667 Y38.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y86.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y121.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X29.500000 Y129.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X39.333333 Y130.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X44.666667 Y139.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X35.166667 Y140.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X40.666667 Y148.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X37.833333 Y157.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X26.500000 Y159.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X31.666667 Y167.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y176.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X27.833333 Y186.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X33.833333 Y194.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y200.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X32.500000 Y207.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y215.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X30.666667 Y223.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y230.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.166667 Y240.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X29.666667 Y249.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y257.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X35.000000 Y264.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X40.166667 Y255.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X40.833333 Y245.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X45.166667 Y237.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X54.333333 Y238.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X59.166667 Y246.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X50.500000 Y250.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X56.500000 Y258.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.166667 Y263.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X43.833333 Y272.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.666667 Y281.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X35.166667 Y275.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X26.500000 Y279.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X34.000000 Y285.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.500000 Y289.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X33.000000 Y297.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y302.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X33.500000 Y309.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y313.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X32.166667 Y319.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.500000 Y326.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X32.500000 Y333.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X26.666667 Y341.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X32.833333 Y351.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.166667 Y356.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X32.500000 Y362.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y368.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X31.500000 Y375.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X39.000000 Y369.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X46.666667 Y363.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X41.833333 Y355.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.000000 Y348.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X57.166667 Y343.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X61.500000 Y351.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X69.166667 Y346.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X71.000000 Y355.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X65.166667 Y337.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X56.500000 Y329.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.833333 Y335.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X39.500000 Y341.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X41.500000 Y325.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X49.666667 Y319.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X56.500000 Y313.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.833333 Y307.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X41.666667 Y314.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X41.000000 Y302.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.333333 Y293.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X50.500000 Y297.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X58.500000 Y293.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X66.500000 Y297.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X58.500000 Y302.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X65.333333 Y309.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X75.000000 Y313.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X74.833333 Y302.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X74.500000 Y292.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X68.000000 Y285.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X61.000000 Y279.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X52.000000 Y276.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X52.166667 Y285.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X57.000000 Y268.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X67.500000 Y261.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X67.000000 Y252.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X77.000000 Y257.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X86.500000 Y257.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X94.000000 Y251.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X88.166667 Y244.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X80.666667 Y238.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X83.833333 Y229.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X89.000000 Y221.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X85.500000 Y213.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X76.666667 Y213.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.833333 Y219.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X79.500000 Y221.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X74.333333 Y230.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X63.166667 Y234.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X62.000000 Y224.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X56.666667 Y217.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.333333 Y221.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X53.166667 Y229.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X41.500000 Y227.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X35.833333 Y235.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X40.166667 Y216.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X48.500000 Y211.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X57.166667 Y207.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X62.666667 Y200.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X72.000000 Y201.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X81.166667 Y203.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X90.333333 Y205.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X94.500000 Y214.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X103.000000 Y218.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X99.833333 Y226.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X92.666667 Y233.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X97.333333 Y241.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X103.166667 Y249.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X111.333333 Y244.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X110.166667 Y258.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X114.333333 Y267.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X105.666667 Y271.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X98.833333 Y277.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X94.500000 Y286.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X85.166667 Y288.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X84.666667 Y299.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X93.666667 Y296.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X103.333333 Y290.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X110.333333 Y280.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X118.666667 Y276.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X122.500000 Y284.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X128.666667 Y255.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X100.000000 Y260.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X93.000000 Y267.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X87.333333 Y276.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X78.000000 Y279.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X79.666667 Y267.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X69.500000 Y271.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X79.166667 Y248.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X69.666667 Y241.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X66.500000 Y211.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X51.666667 Y197.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X56.500000 Y188.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X63.000000 Y181.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X67.166667 Y190.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X73.333333 Y181.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X81.166667 Y175.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X84.666667 Y184.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X94.166667 Y184.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X101.166667 Y177.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X106.500000 Y170.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X114.500000 Y165.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X123.333333 Y162.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X117.166667 Y154.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X108.833333 Y149.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X105.500000 Y158.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X96.000000 Y165.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X94.000000 Y156.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X97.166667 Y147.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X89.500000 Y142.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X84.666667 Y150.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X79.000000 Y142.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X70.666667 Y146.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X62.000000 Y141.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X53.166667 Y145.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X60.666667 Y151.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X68.333333 Y157.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X77.833333 Y156.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X85.333333 Y162.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X75.333333 Y165.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X67.166667 Y170.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X57.500000 Y170.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X59.833333 Y161.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X50.166667 Y155.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X47.500000 Y164.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.000000 Y172.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X49.333333 Y178.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X43.333333 Y188.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X36.000000 Y180.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.166667 Y201.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X78.500000 Y191.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X88.500000 Y195.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X99.333333 Y197.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X108.666667 Y198.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X104.500000 Y189.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X112.000000 Y181.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X117.333333 Y174.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X126.500000 Y172.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.333333 Y165.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X141.666667 Y155.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.000000 Y159.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X165.666667 Y165.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X167.833333 Y174.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X172.333333 Y180.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X177.166667 Y188.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X161.666667 Y180.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X159.000000 Y172.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X148.500000 Y191.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X162.166667 Y202.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X167.166667 Y207.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X188.666667 Y223.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X160.666667 Y265.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X163.166667 Y275.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X111.166667 Y213.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X117.500000 Y206.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X102.166667 Y208.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X90.333333 Y173.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X115.333333 Y142.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X123.666667 Y134.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X121.333333 Y111.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X102.500000 Y110.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X147.833333 Y66.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X146.333333 Y41.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X137.333333 Y37.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X155.500000 Y35.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X171.666667 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X176.333333 Y33.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X182.500000 Y40.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X192.000000 Y41.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X197.666667 Y33.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X193.000000 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X187.166667 Y32.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X181.333333 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X202.666667 Y25.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X201.833333 Y44.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X189.166667 Y50.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X236.333333 Y67.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X176.666667 Y144.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X167.833333 Y142.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X119.500000 Y27.000000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.500000 Y140.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X28.666667 Y149.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.166667 Y269.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X62.833333 Y320.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X98.000000 Y339.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X132.333333 Y350.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X149.166667 Y381.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X127.333333 Y412.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X87.333333 Y376.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X47.666667 Y373.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X39.333333 Y381.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X32.000000 Y388.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X36.333333 Y396.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X26.833333 Y396.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X33.333333 Y406.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X42.500000 Y408.666667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X49.333333 Y401.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y413.333333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X25.000000 Y381.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X28.333333 Y460.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X28.166667 Y468.500000 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X26.333333 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X36.833333 Y474.833333 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
G1 X34.333333 Y466.166667 F3000 ; go to (X, Y) with speed (F)
M106 S255 ; turn on drill
G1 Z2.000000 F190 ; go to (Z) with speed (F)
G1 Z9.000000 F800 ; go to (Z) with speed (F)
G1 Z13.000000 F800 ; go to (Z) with speed (F)
M106 S100 ; turn on drill
