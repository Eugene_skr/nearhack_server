import json
from http.server import BaseHTTPRequestHandler, HTTPServer
import hashlib
import base64
from PIL import Image
from io import BytesIO
import os

hostName = ""
serverPort = 9090
ninjaBinPath = "/Users/eugene/Documents/projects/ninja_art/source/StringArt/cmake-build-release/NinjaArt"

ninjaInputPath = "/Users/eugene/Documents/projects/ninja_art/source/StringArt/Repository/Input"
ninjaInputImage = "/Users/eugene/Documents/projects/ninja_art/source/StringArt/Repository/RawImages"
ninjaOutputPath = "/Users/eugene/Documents/projects/ninja_art/source/StringArt/output/"


def read_settings_for_token(token_id):
    with open("./requests/" + token_id + ".json", "r") as f:
        inp = json.load(f)
        # "width": "50", "": "50", "border": 2, "isUpload": true}}
        return {'width': inp['data']['width'],
                'height': inp['data']['height'],
                'style': inp['data']['style']}


def prepare_input_json(sample_filepath, token_id, img_filename, width, height, style):
    # check for default
    if sample_filepath == "":
        sample_filepath = "./samples/nearwin.json"
    if style == 0:
        style = 0.95
    if height == 0:
        height = 500

    # read smaple json
    with open(sample_filepath, "r") as f:
        inp = json.load(f)
        for i in [0, 1]:
            inp['image_settings']['settings_array'][i]['image_filename'] = img_filename
            inp['image_settings']['settings_array'][i]['out_mm_height'] = int(height) * 10

        inp['string_placer_settings']['settings_array'][0]['distance_pow'] = float(style)

    # write to file
    savepath = ninjaInputPath + "/" + token_id + ".json"
    with open(savepath, "w") as f:
        json.dump(inp, f)

    #copy image
    os.system("cp " + "./inputs/" + img_filename + " " + ninjaInputImage + "/" + img_filename)
    return


def remove_char(string):
    return ''.join(e for e in string if e.isalnum())


def parse_input_json(token_id):
    with open("./requests/" + token_id + ".json", "r") as f:
        inp = json.load(f)
        image = inp['data']['media'].split(',')[1]
        im = Image.open(BytesIO(base64.b64decode(image)))
        filename = "./inputs/" + token_id + '.jpeg'
        with open(filename, "wb") as fp:
            im.save(fp)
            fp.close()
        return


class NinjaArtServer(BaseHTTPRequestHandler):
    def return_err(self):
        self.send_response(404)
        self.end_headers()
        return

    def return_file(self, path, name):
        self.send_response(200)
        self.send_header('Content-type', 'application/octet-stream')
        self.send_header('Content-Disposition', 'attachment; filename=' + name)
        self.end_headers()
        with open(path, "rb") as f:
            self.wfile.write(f.read())
        return

    def do_GET(self):
        real_path = remove_char(self.path).lower()

        if real_path[0: len("coordinates")] == "coordinates":
            token = real_path[len("coordinates"): len(real_path)]
            path = ninjaOutputPath + token + "/"
            isdir = os.path.isdir(path)
            if isdir:
                self.return_file(ninjaOutputPath + token + "/nearwin_3dinfo.json", "nft.json")
            else:
                self.return_err()
            return

        if real_path[0: len("imgfull")] == "imgfull":
            token = real_path[len("imgfull"): len(real_path)]
            path = ninjaOutputPath + token + "/"
            isdir = os.path.isdir(path)
            if isdir:
                self.return_file(ninjaOutputPath + token + "/nearwin_result.png", "img.png")
            else:
                self.return_err()
            return

        if real_path[0: len("gcode")] == "gcode":
            token = real_path[len("gcode"): len(real_path)]
            path = ninjaOutputPath + token + "/"
            isdir = os.path.isdir(path)
            if isdir:
                self.return_file(ninjaOutputPath + token + "/nearwin_strings.gcode", "cnc.gcode")
            else:
                self.return_err()
            return

    def _set_response(self, ctype):
        self.send_response(200)
        self.send_header('Content-type', ctype)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()

    def do_nft(self, post_body):
        token = hashlib.md5(post_body).hexdigest()
        with open("./requests/" + token + ".json", "wb") as f:
            f.write(post_body)
        parse_input_json(token)
        self._set_response('text/html')
        self.wfile.write(json.dumps({'hash': token}).encode('utf-8'))

        settings = read_settings_for_token(token)
        prepare_input_json("./samples/nearwin.json",
                           token,
                           token + '.jpeg',
                           settings['width'],
                           settings['height'],
                           settings['style'])

        os.system("./bin/bin.sh " + token + " " + ninjaOutputPath + token + "/")
        return

    def do_POST(self):
        real_path = remove_char(self.path)
        content_len = int(self.headers.get('Content-Length'))
        post_body = self.rfile.read(content_len)
        if real_path.lower() == "newnft":
            self.do_nft(post_body)
            return

    def do_OPTIONS(self):
        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, OPTIONS, POST')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
        self.send_header("Access-Control-Allow-Headers", "Content-Type")
        self.end_headers()
        return


if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), NinjaArtServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
